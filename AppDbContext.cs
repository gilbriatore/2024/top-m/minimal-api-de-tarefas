
using Microsoft.EntityFrameworkCore;

class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options)
       : base(options) { }

    //Sobrescrita do método necessário para conexão com MySQL
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseMySQL(
            "server=localhost;port=3306;database=tarefas;user=root;password=positivo");
    }

    //Tabelas do banco de dados
    public DbSet<Tarefa> Tarefas => Set<Tarefa>();

}